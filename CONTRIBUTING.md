# Contributing

- Create a new branch

    ```
    git checkout -b my-awesome-branch
    ```

- Make unit test that describe feature or testing the issue

- Code it!

- Commit the changes

    ```
    git add .
    git commit -am "a very awesome commit"
    ```

- Repeat code and commit

- Run the test, fix all failed specs

    ```
    bundle exec rspec
    ```

- Push when its ready

    ```
    git push origin my-awesome-branch
    ```

- Create a merge request

- Ask someone to review and qa your pull request, repeat bugfix until merged

- Good job!
