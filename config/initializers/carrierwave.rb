CarrierWave.configure do |config|
  config.fog_provider = 'fog/google'
  config.fog_credentials = {
    provider: 'Google',
    google_storage_access_key_id: ENV.fetch('FOG_GOOGLE_ACCESS_KEY'),
    google_storage_secret_access_key: ENV.fetch('FOG_GOOGLE_SECRET')
  }
  config.fog_directory = ENV.fetch('FOG_DIRECTORY')
end
