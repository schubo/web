Rails.application.routes.draw do
  devise_for :accounts

  root 'pages#home'

  mount LetterOpenerWeb::Engine, at: '/letter_opener' if Rails.env.development?

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      devise_for :accounts,
                 path: '/',
                 path_names: {
                   sign_in: 'login',
                   sign_out: 'logout'
                 },
                 sign_out_via: [:delete, :get]

      resources :schools, only: [:index]
      resource :parents, only: [:show, :update]
      resources :children, only: [:index, :show, :create, :update, :destroy] do
        post :pair
        post :board
      end
      resources :bookings, only: [:index, :show, :create, :update] do
        post :cancel
        post :finish
        post :approach
      end
      resources :meeting_points, only: [:index]
      resources :devices, only: [:create]
    end
  end

  resources :dashboards, only: [:index]

  scope :accounts do
    resource :invitation, controller: 'invitations', only: [:new, :create, :update] do
      collection do
        get :accept, action: :edit
      end
    end
  end

  namespace :pages do
    get :home
  end
end
