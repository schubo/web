# Schubo

## Description

Schubo web is a main repository for Schubo App. It consist registration page for School and Parent, it also consist all API related to the services.

## Architecture Diagram

TODO

## Onboarding and Development Guide

### Installation

#### Install Dependencies

1. [MySQL](https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en/)
2. [Node.js](https://nodejs.org/en/download/)
3. [Yarn](https://yarnpkg.com/lang/en/docs/install/)

#### Install RVM & Ruby

1. RVM

    ```
    $ curl -L get.rvm.io | bash -s stable
    $ source ~/.bash_profile
    ```

    Then run this command and follow the instructions

    ```
    rvm requirements
    ```

2. Ruby 2.5.3

    ```
    $ rvm install 2.5.3
    ```

#### Setup Schubo

1. Clone Repository

    ```
    $ git clone git@gitlab.com:schubo/web.git
    $ cd web
    ```

2. Install Bundler

    ```
    $ gem install bundler
    ```

3. Install Gems and NPM Dependencies

    ```
    $ bundle install
    $ yarn
    ```

4. Copy `env` file

    ```
    $ cp env.sample .env
    ```

  Then make any changes if required.

5. Prepare Databases

    ```
    $ rake db:create:all
    $ rake db:schema:load
    $ rake db:seed
    ```

  Then make any changes if required.

#### Run Schubo

1. Run all dependencies services like MySQL, etc.

2. Start Server

    ```
    $ rails s
    ```

3. Access it at `http://localhost:3000`

### Contributing

Read all documents below
- [How To Contribute](CONTRIBUTING.md)
- [Ruby Style Guide](https://github.com/bbatsov/ruby-style-guide)
- [Rails Style Guide](https://github.com/bbatsov/rails-style-guide)

## Links

Useful Link:

- [Ruby](https://www.ruby-lang.org/en/)
- [Ruby on Rails](http://rubyonrails.org/)
- [Bundler](http://bundler.io/)
- [MySQL](https://www.mysql.com/)
