# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

FactoryBot.create(:account, :admin, email: 'ali.ariff12@gmail.com')
acc_with_school = FactoryBot.create(:account, :with_school)
acc_with_parent = FactoryBot.create(:account, :with_parent, email: 'leonardo@hotmail.com')
registered_parent = FactoryBot.create(:account, :registered_parent)

school = acc_with_school.entity
school.update(name: 'Auf Der Hörn Elementary School', street: 'Ahornstraße', street_number: '60', zipcode: '52074', town: 'Aachen, Germany')

parent = acc_with_parent.entity
parent.update(street: 'Trierer Str', street_number: '150', zipcode: '52078', town: 'Aachen, Germany')

other_school = FactoryBot.create(:account, :with_school).entity
other_school.update(name: 'KGS Beeckstraße', street: 'Beeckstraße', street_number: '15-17', zipcode: '52062', town: 'Aachen, Germany')

parent.schools << school
parent.schools << other_school
parent.children << FactoryBot.create(:child, school: school)
parent.children << FactoryBot.create(:child, school: school)
parent.children << FactoryBot.create(:child, school: other_school, active: false)

parent.devices << FactoryBot.create(:device)
parent.children.where(active: true).each do |child|
  child.devices << FactoryBot.create(:device)
end

FactoryBot.create(:meeting_point, name: 'Aachen Arkaden', street: 'Trierer Str', street_number: '1', zipcode: '52078', town: 'Aachen, Germany')

p "parent: #{acc_with_parent.email}"
