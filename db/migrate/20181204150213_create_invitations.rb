class CreateInvitations < ActiveRecord::Migration[5.2]
  def change
    create_table :invitations do |t|
      t.integer :account_id
      t.integer :invited_by_id
      t.string :token
      t.datetime :accepted_at
      t.integer :school_id
      t.integer :invited_as
      t.timestamps null: false
    end
    add_index :invitations, [:account_id, :invited_by_id], unique: true
  end
end
