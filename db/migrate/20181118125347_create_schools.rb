class CreateSchools < ActiveRecord::Migration[5.2]
  def change
    create_table :schools do |t|
      t.string :name
      t.string :street
      t.string :street_number
      t.string :zipcode
      t.string :town
      t.string :phone_no
      t.float :latitude
      t.float :longitude
      t.timestamps null: false
    end
  end
end
