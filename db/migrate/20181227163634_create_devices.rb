class CreateDevices < ActiveRecord::Migration[5.2]
  def change
    create_table :devices do |t|
      t.string :token
      t.integer :os
      t.string :owner_type
      t.string :owner_id
      t.timestamps null: false
    end

    add_index :devices, :token, unique: true
    add_index :devices, [:owner_type, :owner_id]
  end
end
