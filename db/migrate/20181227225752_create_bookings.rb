class CreateBookings < ActiveRecord::Migration[5.2]
  def change
    create_table :bookings do |t|
      t.integer :parent_id, index: true
      t.integer :school_id, index: true
      t.integer :meeting_point_id
      t.integer :status
      t.integer :trip_type
      t.date :date
      t.timestamps null: false
    end

    create_table :bookings_children, id: false do |t|
      t.integer :booking_id, index: true
      t.string :child_id, index: true
    end
  end
end
