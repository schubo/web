class CreateSchoolMemberships < ActiveRecord::Migration[5.2]
  def change
    create_table :school_memberships do |t|
      t.integer :school_id
      t.integer :parent_id
      t.timestamps null: false
    end
    add_index :school_memberships, [:school_id, :parent_id], unique: true
  end
end
