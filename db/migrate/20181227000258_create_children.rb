class CreateChildren < ActiveRecord::Migration[5.2]
  def change
    create_table :children, id: false do |t|
      t.string :id, limit: 36, primary_key: true, null: false
      t.integer :school_id
      t.string :name
      t.string :avatar
      t.integer :stage
      t.boolean :active, default: false
      t.timestamps null: false
    end

    create_table :children_parents do |t|
      t.string :child_id, index: true
      t.integer :parent_id, index: true
      t.timestamps null: false
    end

    add_index :children_parents, [:child_id, :parent_id], unique: true
  end
end
