class CreateMeetingPoints < ActiveRecord::Migration[5.2]
  def change
    create_table :meeting_points do |t|
      t.string :name
      t.string :street
      t.string :street_number
      t.string :zipcode
      t.string :town
      t.float :latitude
      t.float :longitude
      t.timestamps null: false
    end
  end
end
