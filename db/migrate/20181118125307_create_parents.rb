class CreateParents < ActiveRecord::Migration[5.2]
  def change
    create_table :parents do |t|
      t.string :first_name
      t.string :last_name
      t.string :street
      t.string :street_number
      t.string :zipcode
      t.string :town
      t.string :phone_no
      t.timestamps null: false
    end
  end
end
