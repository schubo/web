require 'rails_helper'

describe InvitationMailer do
  let(:invitation) { create :invitation, :as_school }

  describe '#invitation_instructions' do
    let(:mail) { InvitationMailer.invitation_instructions(invitation) }

    it {
      expect(mail[:from].value).to eq "no-reply@#{ENV.fetch('PRODUCTION_URL')}"
      expect(mail.subject).to eq 'Invitation instructions'
      expect(mail.body.raw_source).to include accept_invitation_url
    }
  end
end
