require 'rails_helper'

RSpec.describe InvitationsController, type: :request do
  describe 'GET /accounts/invitation/new' do
    let(:account) { create :account, :admin }
    before do
      sign_in account
    end

    it 'returns http success' do
      get new_invitation_path
      expect(response).to have_http_status(:success)
    end
  end

  describe 'POST /accounts/invitation' do
    let(:account) { create :account, :admin }
    before do
      sign_in account
    end

    it 'create new invitation' do
      expect do
        post invitation_path, params: { account: { email: 'test@abc.com' } }
      end.to change { Invitation.count }.by(1)

      expect(response.status).to eq(302)
    end

    it 'failed to create' do
      expect do
        post invitation_path, params: { account: { email: '' } }
      end.to change { Invitation.count }.by(0)

      expect(response.status).to eq(302)
    end
  end

  describe 'GET /accounts/invitation/accept' do
    context 'as school' do
      let(:invitation) { create :invitation, :as_school }

      it 'returns http success' do
        get accept_invitation_path, params: { token: invitation.token }
        expect(response).to have_http_status(:success)
        expect(response.body).to include('Register as new school')
      end

      it 'failed to load page' do
        get accept_invitation_path, params: { token: 'not valid token' }

        expect(response.status).to eq(302)
      end
    end

    context 'as parent' do
      let(:invitation) { create :invitation, :as_parent }

      it 'returns http success' do
        get accept_invitation_path, params: { token: invitation.token }
        expect(response).to have_http_status(:success)
        expect(response.body).to include('Register as parent')
      end

      it 'failed to load page' do
        get accept_invitation_path, params: { token: 'not valid token' }

        expect(response.status).to eq(302)
      end
    end
  end

  describe 'PUT /accounts/invitation' do
    context 'as school' do
      let(:invitation) { create :invitation, :as_school }

      it 'returns http success' do
        expect do
          put invitation_path, params: {
            account: {
              token: invitation.token,
              password: 'test123',
              password_confirmation: 'test123',
              entity: {
                type: 'school',
                name: 'Fun School',
                street: 'Test Street',
                street_number: 123,
                zipcode: 52_077,
                town: 'Aachen',
                phone_no: '0123456789'
              }
            }
          }
        end.to change { School.count }.by(1)

        expect(response.status).to eq(302)
      end

      it 'failed to load page' do
        put invitation_path, params: { account: { token: 'not valid token' } }

        expect(response.status).to eq(302)
      end
    end

    context 'as parent' do
      let(:invitation) { create :invitation, :as_parent }

      it 'returns http success' do
        expect do
          put invitation_path, params: {
            account: {
              token: invitation.token,
              password: 'test123',
              password_confirmation: 'test123',
              entity: {
                type: 'parent',
                first_name: 'John',
                last_name: 'Doe',
                street: 'Test Street',
                street_number: 123,
                zipcode: 52_077,
                town: 'Aachen',
                phone_no: '0123456789'
              }
            }
          }
        end.to change { Parent.count }.by(1)

        expect(response.status).to eq(302)
      end

      it 'failed to load page' do
        put invitation_path, params: { account: { token: 'not valid token' } }

        expect(response.status).to eq(302)
      end

      it 'can show error' do
        account = create :account, :with_school
        invitation = InvitationService.invite(Invitation.new, account, 'test@abc.com')
        expect do
          put invitation_path, params: {
            account: {
              token: invitation.token,
              entity: {
                type: 'parent',
                first_name: 'John',
                last_name: 'Doe',
                street: 'Test Street',
                street_number: 123,
                zipcode: 52_077,
                town: 'Aachen',
                phone_no: '0123456789'
              }
            }
          }
        end.to change { Parent.count }.by(0)

        expect(response.status).to eq(200)
        expect(response.body).to include('Password can&#39;t be blank')
      end
    end
  end
end
