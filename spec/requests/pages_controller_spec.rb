require 'rails_helper'

RSpec.describe 'Pages', type: :request do
  describe 'GET /pages/home' do
    it 'returns http success' do
      get pages_home_path
      expect(response).to have_http_status(:success)
    end
  end
end
