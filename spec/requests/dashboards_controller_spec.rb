require 'rails_helper'

RSpec.describe 'Dashboards', type: :request do
  describe 'GET /dashboards' do
    it 'returns http success' do
      sign_in create(:account, :admin)
      get dashboards_path
      expect(response).to have_http_status(:success)
    end

    it 'redirected' do
      get dashboards_path
      expect(response).to have_http_status(302)
    end
  end
end
