FactoryBot.define do
  factory :child do
    school { create :school }
    name { Faker::Name.first_name }
    stage { Faker::Number.between(1, 13) }
    active { true }
  end

  trait :with_parent do
    after(:build) do |child, evaluator|
      child.parents << create(:parent)
    end
  end
end
