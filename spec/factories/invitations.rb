FactoryBot.define do
  factory :invitation do
    token { 'abc123' }

    trait :as_school do
      invited_as { :as_school }
      account { create :account }
      invited_by { create :account, :admin }
    end

    trait :as_parent do
      invited_as { :as_parent }
      account { create :account }
      invited_by { create :account, :with_school }
      school { invited_by.entity }
    end
  end
end
