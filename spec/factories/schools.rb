FactoryBot.define do
  factory :school do
    name { Faker::Company.name }
    street { Faker::Address.street_name }
    street_number { Faker::Address.building_number }
    zipcode { Faker::Address.zip_code }
    town { Faker::Address.city }
    phone_no { Faker::PhoneNumber.phone_number }
  end
end
