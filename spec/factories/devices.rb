FactoryBot.define do
  factory :device do
    token { SecureRandom.uuid }
    os { [:android, :ios].sample }
    owner { create :parent }
  end
end
