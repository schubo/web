FactoryBot.define do
  factory :account do
    email { Faker::Internet.unique.free_email }
    password { 'test123' }

    trait :admin do
      admin { true }
    end

    trait :with_school do
      entity { create :school }
    end

    trait :with_parent do
      entity { create :parent }
    end

    trait :registered_parent do
      with_parent

      after(:build) do |account, evaluator|
        account.entity.schools << create(:school)
      end
    end
  end
end
