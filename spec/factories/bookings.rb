FactoryBot.define do
  factory :booking do
    date { Faker::Date.between(10.days.ago, 10.days.from_now) }
    trip_type { :both_ways }
    status { :active }

    trait :with_association do
      after(:build) do |booking, evaluator|
        account = create(:account, :registered_parent)
        parent = account.entity
        school = parent.schools.first
        child = create(:child, school: school)
        parent.children << child

        booking.parent = parent
        booking.school = school
        booking.meeting_point = create(:meeting_point)
        booking.children << child
        booking.save!
      end
    end
  end
end
