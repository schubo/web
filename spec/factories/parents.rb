FactoryBot.define do
  factory :parent do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    street { Faker::Address.street_name }
    street_number { Faker::Address.building_number }
    zipcode { Faker::Address.zip_code }
    town { Faker::Address.city }
    phone_no { Faker::PhoneNumber.cell_phone }
  end
end
