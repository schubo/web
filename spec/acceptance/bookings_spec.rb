require 'rails_helper'

resource 'Bookings' do
  explanation 'Endpoint for booking resources'

  header 'Content-Type', 'application/json'
  header 'Authorization', :authorization_header

  let(:authorization_header) { authorization_header_for(account) }
  let(:account) { create :account, :registered_parent }
  let(:parent) { account.entity }
  let(:child) { create :child, school: school }
  let(:school) { create :school }
  let(:meeting_point) { create :meeting_point }
  let!(:booking) { create :booking, school: school, parent: parent, children: [child], meeting_point: meeting_point }

  before do
    parent.children << child
    parent.schools << school
  end

  get '/api/v1/bookings' do
    context '200' do
      example_request 'Get all bookings (with parent token)' do
        explanation 'List of all bookings scoped to the current user (parent)'

        expect(status).to eq(200)
        expect(json[0]['id']).to eq(booking.id)
      end
    end

    context '200' do
      let(:authorization_header) { authorization_header_for(child) }

      example_request 'Get all bookings (with child token)' do
        explanation 'List of all bookings scoped to the current user (child)'

        expect(status).to eq(200)
        expect(json[0]['id']).to eq(booking.id)
      end
    end
  end

  get '/api/v1/bookings/:id' do
    let(:id) { booking.id }

    context '200' do
      example_request 'Get single booking (with parent token)' do
        explanation 'Detail of single bookings'

        expect(status).to eq(200)
        expect(json['id']).to eq(booking.id)
      end
    end

    context '200' do
      let(:authorization_header) { authorization_header_for(child) }

      example_request 'Get single booking (with child token)' do
        explanation 'Detail of single bookings'

        expect(status).to eq(200)
        expect(json['id']).to eq(booking.id)
      end
    end
  end

  post '/api/v1/bookings' do
    let(:raw_post) { params.to_json }

    with_options scope: :booking do
      parameter :date, 'Date of the booking', required: true
      parameter :trip_type, 'Type of the trip. Supported value: `0` is from meeting point to school, `1` is from school to meeting point, `2` for both ways start from meeting point to school', required: true
      parameter :meeting_point_id, 'Meeting Point ID', required: true
      parameter :child_ids, 'Children ID', required: true
    end

    context '201' do
      let(:date) { Date.today }
      let(:trip_type) { 2 }
      let(:meeting_point_id) { meeting_point.id }
      let(:child_ids) { [create(:child, school: school).id, create(:child, school: school).id] }

      example_request 'Create booking' do
        explanation 'Create a new booking'

        expect(status).to eq(201)
        expect(json['id']).to be_present
      end
    end

    context '422' do
      let(:date) { nil }

      example_request 'Failed to create' do
        expect(status).to eq(422)
      end
    end

    context '422' do
      let(:date) { Date.today }
      let(:trip_type) { 2 }
      let(:meeting_point_id) { meeting_point.id }
      let(:child_ids) { [create(:child).id, create(:child).id] }

      example_request 'Failed to create because of different school' do
        expect(status).to eq(422)
        expect(json['children']).to include 'not all of children is in the same school'
      end
    end
  end

  put '/api/v1/bookings/:id' do
    let(:raw_post) { params.to_json }
    let(:id) { booking.id }

    with_options scope: :booking do
      parameter :date, 'Date of the booking', required: true
      parameter :trip_type, 'Type of the trip. Supported value: `0` is from meeting point to school, `1` is from school to meeting point, `2` for both ways start from meeting point to school', required: true
      parameter :meeting_point_id, 'Meeting Point ID', required: true
      parameter :child_ids, 'Children ID', required: true
    end

    context '200' do
      let(:trip_type) { 1 }

      example_request 'Update booking info' do
        explanation 'Update detail info of the booking'

        expect(status).to eq(200)
        expect(json['id']).to eq(id)
        expect(json['trip_type']).to eq 'school_to_meeting_point'
      end
    end

    context '422' do
      let(:date) { nil }

      example_request 'Failed to update' do
        expect(status).to eq(422)
      end
    end
  end

  post '/api/v1/bookings/:booking_id/cancel' do
    let(:raw_post) { params.to_json }
    let(:booking_id) { booking.id }

    before do
      # update to future date
      booking.update(date: 1.day.from_now)
    end

    context '200' do
      example_request 'Cancel booking' do
        explanation 'Cancel booking'

        expect(status).to eq(200)
        expect(booking.reload.cancelled?).to eq true
      end
    end
  end
end
