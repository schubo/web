require 'rails_helper'

resource 'Authentication' do
  explanation 'Authentication flow with JWT'

  header 'Content-Type', 'application/json'

  let(:raw_post) { params.to_json }
  let(:account) { create :account, :with_parent }

  post '/api/v1/login' do
    with_options scope: :account, required: true do
      parameter :email, 'Email address'
      parameter :password, 'Password'
    end

    let(:email) { account.email }
    let(:password) { account.password }

    context '200' do
      example_request 'Request JWT Token' do
        explanation 'Successful login flow will return JWT token in the response header called `Authorization` key'

        expect(status).to eq(200)
        expect(json['id']).to eq account.id
        expect(json['entity']['type']).to eq 'parent'
        expect(json['entity']['parent']['id']).to eq account.entity.id
        expect(response_headers['Authorization']).to be_present
      end
    end

    context '401' do
      let(:password) { 'wrong password' }

      example_request 'Invalid Credential' do
        expect(status).to eq(401)
        expect(response_headers['Authorization']).to be_nil
      end
    end
  end
end
