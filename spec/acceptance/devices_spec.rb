require 'rails_helper'

resource 'Devices' do
  explanation 'Endpoint for devices resources'

  header 'Content-Type', 'application/json'
  header 'Authorization', :authorization_header

  let(:account) { create :account, :registered_parent }
  let(:authorization_header) { authorization_header_for(account) }
  let(:raw_post) { params.to_json }

  post '/api/v1/devices' do
    with_options scope: :device do
      parameter :token, 'Google Firebase token or Apple device token', required: true
      parameter :os, 'The os of the device. Support `android` or `ios`', required: true
    end

    context '201' do
      let(:token) { SecureRandom.uuid }
      let(:os) { :android }

      example_request 'Register a new device' do
        explanation 'Every time user login to a new device it should be tracked'

        expect(status).to eq(201)
        device = Device.last
        expect(device.android?).to eq true
        expect(device.owner).to eq account.entity
      end
    end

    context '422' do
      let(:token) { nil }
      let(:os) { nil }

      example_request 'Unprocessable entity' do
        expect(status).to eq(422)
      end
    end
  end
end
