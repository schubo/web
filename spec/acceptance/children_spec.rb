require 'rails_helper'

resource 'Children' do
  explanation 'Endpoint for children resources'

  header 'Content-Type', 'application/json'
  header 'Authorization', :authorization_header

  let(:authorization_header) { authorization_header_for(account) }
  let(:account) { create :account, :registered_parent }
  let(:parent) { account.entity }
  let(:child) { create :child, school: school }
  let(:school) { create :school }

  before do
    parent.children << child
    parent.schools << school
  end

  get '/api/v1/children' do
    context '200' do
      example_request 'Get all children' do
        explanation 'List of all children scoped to the current user (parent)'

        expect(status).to eq(200)
        expect(json[0]['id']).to eq(child.id)
      end
    end
  end

  get '/api/v1/children/:id' do
    let(:id) { child.id }

    context '200' do
      example_request 'Get single children (with parent token)' do
        explanation 'Detail of single children'

        expect(status).to eq(200)
        expect(json['id']).to eq(child.id)
      end
    end

    context '200' do
      let(:authorization_header) { authorization_header_for(child) }

      example_request 'Get single children (with child token)' do
        explanation 'Retrieve the child own data'

        expect(status).to eq(200)
        expect(json['id']).to eq(child.id)
      end
    end
  end

  post '/api/v1/children' do
    header 'Content-Type', 'multipart/form-data'

    with_options scope: :child do
      parameter :name, 'Child name', required: true
      parameter :stage, 'Child education stage/level', required: true
      parameter :school_id, 'School ID', required: true
      parameter :avatar, 'Avatar'
    end

    context '201' do
      let(:name) { 'Mike' }
      let(:stage) { 4 }
      let(:school_id) { school.id }
      let(:avatar) { Rack::Test::UploadedFile.new('spec/fixtures/avatar.png', 'image/png') }

      example_request 'Create child' do
        explanation 'Create a new child'

        expect(status).to eq(201)
        expect(json['id']).to be_present
        expect(json['name']).to eq 'Mike'
        expect(json['avatar_url']).to be_present
        expect(json['school']['id']).to eq school.id
        expect(json['parents'][0]['id']).to eq account.entity.id
      end
    end

    context '422' do
      let(:name) { nil }

      example_request 'Failed to create' do
        expect(status).to eq(422)
      end
    end
  end

  put '/api/v1/children/:id' do
    header 'Content-Type', 'multipart/form-data'
    let(:id) { child.id }

    with_options scope: :child do
      parameter :name, 'Child name', required: true
      parameter :stage, 'Child education stage/level', required: true
      parameter :school_id, 'School ID', required: true
      parameter :avatar, 'Avatar'
    end

    context '200' do
      let(:name) { 'Mike' }
      let(:stage) { 4 }
      let(:school_id) { school.id }
      let(:avatar) { nil }

      example_request 'Update child info' do
        explanation 'Update detail info of the child'

        expect(status).to eq(200)
        expect(json['id']).to eq(id)
        expect(json['name']).to eq 'Mike'
      end
    end

    context '422' do
      let(:name) { nil }

      example_request 'Failed to update' do
        expect(status).to eq(422)
      end
    end
  end

  delete '/api/v1/children/:id' do
    let(:id) { child.id }
    let!(:booking) {
      booking = create :booking, :with_association
      booking.children = [child]
      booking.save!
      booking
    }

    context '200' do
      example_request 'Delete child' do
        explanation 'Removing child'

        expect(status).to eq(200)
        expect { child.reload }.to raise_error ActiveRecord::RecordNotFound
        expect(Booking.count).to eq 0
      end
    end
  end

  post '/api/v1/children/:child_id/pair' do
    let(:raw_post) { params.to_json }
    let(:child) { create :child, school: school, active: false }
    let(:child_id) { child.id }
    let(:authorization_header) { '' }
    let(:token) { SecureRandom.uuid }
    let(:os) { :android }

    with_options scope: :device do
      parameter :token, 'Google Firebase token or Apple device token', required: true
      parameter :os, 'The os of the device. Support `android` or `ios`', required: true
    end

    context '200' do
      example_request 'Pairing phone' do
        explanation 'Paring children phone with parent account and in return JWT token in the response header called `Authorization` key'

        expect(status).to eq(200)
        expect(child.reload.active).to eq true
        device = Device.last
        expect(device.android?).to eq true
        expect(device.owner).to eq child
        expect(response_headers['Authorization']).to be_present
      end
    end
  end
end
