require 'rails_helper'

resource 'Parents' do
  explanation 'Endpoint for parent resources'

  header 'Content-Type', 'application/json'
  header 'Authorization', :authorization_header

  let(:authorization_header) { authorization_header_for(account) }

  get '/api/v1/parents' do
    context '200' do
      let(:account) { create :account, :registered_parent }

      example_request 'Get current info of parent' do
        explanation 'Detail info of parent scoped to the current user'

        expect(status).to eq(200)
        expect(json['id']).to eq(account.entity.id)
      end
    end

    context '401' do
      let(:account) { create :account, :with_school }

      example_request 'Unauthorized' do
        expect(status).to eq(401)
      end
    end
  end

  put '/api/v1/parents' do
    let(:raw_post) { params.to_json }
    let(:account) { create :account, :registered_parent }

    with_options scope: :parent do
      parameter :first_name, 'First name'
      parameter :last_name, 'Last name'
      parameter :street, 'Street name of the house'
      parameter :street_number, 'House number'
      parameter :zipcode, 'ZIP code'
      parameter :town, 'City'
      parameter :phone_no, 'Phone number'
    end

    context '200' do
      let(:first_name) { 'John' }
      let(:last_name) { 'Doe' }
      let(:street) { 'Some Str' }
      let(:street_number) { '123' }
      let(:zipcode) { '52066' }
      let(:town) { 'Aachen' }
      let(:phone_no) { '017612345' }

      example_request 'Update parent info' do
        explanation 'Update detail info of the parent'

        expect(status).to eq(200)
        expect(json['first_name']).to eq 'John'
        expect(json['last_name']).to eq 'Doe'
        expect(json['phone_no']).to eq '017612345'
      end
    end

    context '422' do
      let(:first_name) { nil }
      let(:last_name) { nil }

      example_request 'Unprocessable entity' do
        expect(status).to eq(422)
      end
    end
  end
end
