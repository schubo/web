require 'rails_helper'

resource 'Schools' do
  explanation 'Endpoint for school resources'

  header 'Content-Type', 'application/json'
  header 'Authorization', :authorization_header

  let(:authorization_header) { authorization_header_for(account) }

  get '/api/v1/schools' do
    context '200' do
      let(:account) { create :account, :registered_parent }

      example_request 'Get list of schools' do
        explanation 'List all of schools that the current user is members on'

        expect(status).to eq(200)
        expect(json[0]['id']).to eq(account.entity.schools.first.id)
      end
    end

    context '401' do
      let(:account) { create :account, :with_school }

      example_request 'Unauthorized' do
        expect(status).to eq(401)
      end
    end
  end
end
