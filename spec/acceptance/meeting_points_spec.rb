require 'rails_helper'

resource 'Meeting Points' do
  explanation 'Endpoint for meeting point resources'

  header 'Content-Type', 'application/json'
  header 'Authorization', :authorization_header

  let(:authorization_header) { authorization_header_for(account) }

  get '/api/v1/meeting_points' do
    context '200' do
      let!(:meeting_point) { create :meeting_point }
      let(:account) { create :account, :registered_parent }

      example_request 'Get list of meeting points' do
        explanation 'List all of meeting points that available'

        expect(status).to eq(200)
        expect(json.count).to eq(1)
      end
    end
  end
end
