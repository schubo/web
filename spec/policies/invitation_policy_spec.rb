require 'rails_helper'

describe InvitationPolicy do
  subject { InvitationPolicy.new account, invitation }
  let(:invitation) { create :invitation, :as_parent }

  context 'as admin' do
    let(:account) { create :account, :admin }
    it {
      is_expected.to permit_action :create
      is_expected.to permit_action :update
    }
  end

  context 'as school' do
    let(:account) { create :account, :with_school }
    it {
      is_expected.to permit_action :create
      is_expected.to permit_action :update
    }
  end

  context 'as parent' do
    let(:account) { create :account, :with_parent }
    it {
      is_expected.not_to permit_action :create
      is_expected.to permit_action :update
    }
  end
end
