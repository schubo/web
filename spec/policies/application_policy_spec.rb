require 'rails_helper'

describe ApplicationPolicy do
  subject { ApplicationPolicy.new account, Account }
  let(:account) { create :account, :admin }
  let(:scope) { ApplicationPolicy::Scope.new(account, Account).resolve }

  it {
    is_expected.not_to permit_action :index
    is_expected.not_to permit_action :show
    is_expected.not_to permit_action :create
    is_expected.not_to permit_action :new
    is_expected.not_to permit_action :update
    is_expected.not_to permit_action :edit
    is_expected.not_to permit_action :destroy

    expect(scope).to include account
  }
end
