require 'rails_helper'

RSpec.describe InvitationService::Accept, type: :model do
  let(:email) { 'test@abc.com' }

  context 'as school' do
    let(:account) { create :account, :admin }
    let(:invitation) do
      InvitationService.invite(Invitation.new, account, email)
    end
    let(:params) do
      {
        password: 'test123',
        password_confirmation: 'test123',
        entity: {
          type: 'school',
          name: 'Fun School',
          street: 'Test Street',
          street_number: 123,
          zipcode: 52_077,
          town: 'Aachen',
          phone_no: '0123456789'
        }
      }.with_indifferent_access
    end

    it 'run successfully' do
      expect do
        @result = InvitationService.accept(invitation, Account.new, params)
      end.not_to raise_error

      expect(@result.accepted_at).to be_present
      expect(@result.account.entity.name).to eq 'Fun School'
    end
  end

  context 'as parent' do
    let(:account) { create :account, :with_school }
    let(:invitation) do
      InvitationService.invite(Invitation.new, account, email)
    end
    let(:params) do
      {
        password: 'test123',
        password_confirmation: 'test123',
        entity: {
          type: 'parent',
          first_name: 'John',
          last_name: 'Doe',
          street: 'Test Street',
          street_number: 123,
          zipcode: 52_077,
          town: 'Aachen',
          phone_no: '0123456789'
        }
      }.with_indifferent_access
    end

    it 'run successfully' do
      expect do
        @result = InvitationService.accept(invitation, Account.new, params)
      end.not_to raise_error

      expect(@result.accepted_at).to be_present
      expect(@result.account.entity.first_name).to eq 'John'
      expect(@result.account.entity.schools).to include @result.school
      expect(@result.school.parents).to include @result.account.entity
    end
  end
end
