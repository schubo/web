require 'rails_helper'

RSpec.describe InvitationService::Validate, type: :model do
  let(:account) { create :account, :admin }

  it 'run successfully' do
    invitation = create :invitation, :as_school
    expect do
      InvitationService.validate(invitation, account)
    end.not_to raise_error
  end

  it 'raise error when token invalid' do
    expect do
      InvitationService.validate(Invitation.new, account)
    end.to raise_error InvitationService::InvalidToken
  end

  it 'raise error when invitation already accepted' do
    invitation = create :invitation, :as_parent, accepted_at: Time.zone.now
    expect do
      InvitationService.validate(invitation, account)
    end.to raise_error InvitationService::AlreadyAcceptedError
  end

  it 'raise error when invitation open by the inviter' do
    invitation = create :invitation, :as_school
    expect do
      InvitationService.validate(invitation, invitation.invited_by)
    end.to raise_error Services::InvalidActor
  end
end
