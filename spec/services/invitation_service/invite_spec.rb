require 'rails_helper'

RSpec.describe InvitationService::Invite, type: :model do
  let(:email) { 'test@abc.com' }

  subject do
    InvitationService.invite(Invitation.new, account, email)
  end

  context 'as admin' do
    let(:account) { create :account, :admin }
    let(:email) { account.email }

    it 'can not invite existing admin' do
      expect { subject }.to raise_error InvitationService::AlreadyInvitedError
    end
  end

  context 'as school' do
    let(:account) { create :account, :admin }

    it 'successfully invite a user as a school' do
      subject
      invitation = Invitation.last
      expect(invitation.account.email).to eq email
      expect(invitation.invited_by).to eq account
      expect(invitation.invited_as).to eq 'as_school'
      email_sent(invitation)
    end

    it 'can not invite twice as a school' do
      another_account = create :account, :admin
      subject
      expect do
        InvitationService.invite(Invitation.new, another_account, email)
      end.to raise_error InvitationService::AlreadyInvitedError
    end
  end

  context 'as parent' do
    let(:account) { create :account, :with_school }

    it 'successfully invite a user as a parent' do
      subject
      invitation = Invitation.last
      expect(invitation.account.email).to eq email
      expect(invitation.school).to eq account.entity
      expect(invitation.invited_by).to eq account
      expect(invitation.invited_as).to eq 'as_parent'
      email_sent(invitation)
    end

    it 'can not invite twice from a same school' do
      subject
      another_account = create :account, entity: account.entity
      expect do
        InvitationService.invite(Invitation.new, another_account, email)
      end.to raise_error InvitationService::AlreadyInvitedError
    end

    it 'can invite twice as a school' do
      another_account = create :account, :with_school
      subject
      invitation = Invitation.last
      InvitationService.invite(Invitation.new, another_account, email)

      expect(Invitation.count).to eq 2
      expect(Invitation.last.account).to eq invitation.account
    end
  end

  context 'as parent' do
    let(:account) { create :account, :with_parent }

    it 'will raise error' do
      expect { subject }.to raise_error Services::InvalidActor
    end
  end

  context 'validation' do
    let(:account) { create :account, :with_school }

    it 'raise blank email' do
      expect { InvitationService.invite(Invitation.new, account, '') }.to raise_error InvitationService::EmailMissingError
    end

    it 'can not invite twice' do
      subject
      expect do
        InvitationService.invite(Invitation.new, account, email)
      end.to raise_error InvitationService::AlreadyInvitedError
    end
  end

  def email_sent(invitation)
    expect(enqueued_jobs).to include a_hash_including(
      job: ActionMailer::DeliveryJob,
      queue: 'mailers',
      args: [
        'InvitationMailer',
        'invitation_instructions',
        'deliver_now',
        ActiveJob::Arguments.serialize([invitation]).first
      ]
    )
  end
end
