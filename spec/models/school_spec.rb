require 'rails_helper'

RSpec.describe School, type: :model do
  it {
    is_expected.to have_many :accounts
    is_expected.to have_and_belong_to_many :parents
    is_expected.to have_many :children
  }
end
