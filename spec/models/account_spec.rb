require 'rails_helper'

RSpec.describe Account, type: :model do
  it '#school?' do
    account = create :account, :with_school
    expect(account.school?).to eq true
  end

  it '#parent?' do
    account = create :account, :with_parent
    expect(account.parent?).to eq true
  end
end
