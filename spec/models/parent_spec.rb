require 'rails_helper'

RSpec.describe Parent, type: :model do
  it {
    is_expected.to have_one :account
    is_expected.to have_and_belong_to_many :schools
    is_expected.to have_and_belong_to_many :children
    is_expected.to have_many :devices
  }
end
