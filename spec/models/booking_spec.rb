require 'rails_helper'

RSpec.describe Booking, type: :model do
  it {
    is_expected.to belong_to :school
    is_expected.to belong_to :parent
    is_expected.to have_and_belong_to_many :children
  }
end
