require 'rails_helper'

RSpec.describe Child, type: :model do
  it {
    is_expected.to belong_to :school
    is_expected.to have_and_belong_to_many :parents
    is_expected.to have_many :devices

    expect(subject.save(validate: false)).to eq true
  }
end
