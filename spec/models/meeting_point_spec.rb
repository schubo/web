require 'rails_helper'

RSpec.describe MeetingPoint, type: :model do
  it {
    is_expected.to have_db_column :name
    is_expected.to have_db_column :street
    is_expected.to have_db_column :street_number
    is_expected.to have_db_column :zipcode
    is_expected.to have_db_column :town
    is_expected.to have_db_column :latitude
    is_expected.to have_db_column :longitude
  }
end
