module AuthorizationHeaderHelper
  def authorization_header_for(entity)
    token, payload = if entity.is_a? Account
                       Warden::JWTAuth::UserEncoder.new.call(entity, :account, nil)
                     elsif entity.is_a? Child
                       Warden::JWTAuth::TokenEncoder.new.call(sub: entity.id, scp: :child)
                     end
    "Bearer #{token}"
  end
end
