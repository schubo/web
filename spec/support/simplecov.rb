require 'simplecov'

SimpleCov.start 'rails' do
  add_filter 'vendor'
  add_group 'Services', '/app/services'
  add_group 'Policies', '/app/policies'
end
