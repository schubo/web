class DashboardPolicy < ApplicationPolicy
  def index?
    account.present?
  end
end
