class InvitationPolicy < ApplicationPolicy
  def create?
    account && (account.admin? || account.school?)
  end

  def update?
    true
  end
end
