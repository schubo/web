class API::SchoolPolicy < ApplicationPolicy
  def index?
    account&.parent?
  end
end
