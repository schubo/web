class API::ChildPolicy < ApplicationPolicy
  def index?
    account&.parent?
  end
  alias create? index?

  def show?
    return true if account.nil?

    account&.parent? && @record.parents.where(id: account.entity.id).exists? && # check if the parent is have that children
      account.entity.schools.where(id: @record.school_id).exists? # check if parent are member of their children school
  end

  def update?
    account&.parent? && @record.parents.where(id: account.entity.id).exists? && # check if the parent is have that children
      account.entity.schools.where(id: @record.school_id).exists? # check if parent are member of their children school
  end
  alias destroy? update?

  def pair?
    account.nil? && !@record.active?
  end
end
