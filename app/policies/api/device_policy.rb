class API::DevicePolicy < ApplicationPolicy
  def create?
    account&.parent?
  end
end
