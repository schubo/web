class API::BookingPolicy < ApplicationPolicy
  def index?
    return true if account.nil?

    account&.parent?
  end

  def create?
    account&.parent?
  end

  def show?
    return true if account.nil?

    @record.parent == account.entity
  end

  def update?
    @record.parent == account.entity
  end
  alias destroy? update?

  def cancel?
    account&.parent? && @record.date.future?
  end
end
