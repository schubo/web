class API::ParentPolicy < ApplicationPolicy
  def show?
    account&.parent?
  end

  alias update? show?
end
