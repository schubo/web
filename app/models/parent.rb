class Parent < ApplicationRecord
  has_one :account, as: :entity
  has_and_belongs_to_many :schools, join_table: :school_memberships
  has_and_belongs_to_many :children
  has_many :devices, as: :owner, dependent: :destroy
  has_many :bookings

  validates_presence_of :first_name, :last_name, :street, :street_number, :zipcode, :town, :phone_no
end
