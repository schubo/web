class Booking < ApplicationRecord
  belongs_to :parent
  belongs_to :school
  belongs_to :meeting_point
  has_and_belongs_to_many :children

  enum status: [:active, :cancelled, :finished]
  enum trip_type: [:meeting_point_to_school, :school_to_meeting_point, :both_ways]

  validates_presence_of :parent, :school, :children, :status, :trip_type, :date

  def cancel!
    self.cancelled!
  end

  def finish!
    # :nocov:
    self.finished!
    # :nocov:
  end
end
