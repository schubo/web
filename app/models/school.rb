class School < ApplicationRecord
  has_many :accounts, as: :entity
  has_and_belongs_to_many :parents, join_table: :school_memberships
  has_many :children
  has_many :bookings

  geocoded_by :address
  after_validation :geocode, unless: -> { Rails.env.test? }

  validates_presence_of :name, :street, :street_number, :zipcode, :town, :phone_no

  def address
    ["#{street} #{street_number}", zipcode, town].compact.join(', ')
  end
end
