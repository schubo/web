class MeetingPoint < ApplicationRecord
  geocoded_by :address
  after_validation :geocode, unless: -> { Rails.env.test? }

  validates_presence_of :name, :street, :street_number, :zipcode, :town

  def address
    ["#{street} #{street_number}", zipcode, town].compact.join(', ')
  end
end
