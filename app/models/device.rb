class Device < ApplicationRecord
  belongs_to :owner, polymorphic: true

  validates :token, presence: true, uniqueness: true
  enum os: [:android, :ios]
end
