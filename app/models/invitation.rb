class Invitation < ApplicationRecord
  belongs_to :account
  belongs_to :school, optional: true
  belongs_to :invited_by, class_name: 'Account', foreign_key: :invited_by_id

  enum invited_as: [:as_school, :as_parent]
end
