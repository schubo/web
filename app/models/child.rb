class Child < ApplicationRecord
  belongs_to :school
  has_many :devices, as: :owner, dependent: :destroy
  has_and_belongs_to_many :parents
  has_and_belongs_to_many :bookings
  before_destroy do
    bookings.each(&:destroy)
  end

  before_create :set_uuid

  validates_presence_of :name, :stage, :school

  mount_uploader :avatar, AvatarUploader

  private

  def set_uuid
    self.id = SecureRandom.uuid
  end
end
