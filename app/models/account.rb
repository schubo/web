# :nocov:
class Account < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable,
         :jwt_authenticatable,
         jwt_revocation_strategy: JWTBlacklist

  belongs_to :entity, polymorphic: true, optional: true

  accepts_nested_attributes_for :entity

  def school?
    entity.is_a? School
  end

  def parent?
    entity.is_a? Parent
  end
end
# :nocov:
