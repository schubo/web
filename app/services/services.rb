module Services
  class Base
    include Contracts::Core

    C = Contracts

    def call
      perform
    rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotUnique => e
      raise Services::SystemError, e.message
    end

    def perform
      # :nocov:
      raise NotImplementedError
      # :nocov:
    end

    def initialize(actor = nil)
      @actor = actor
    end
  end

  class ServiceError < StandardError; end

  class SystemError < ServiceError; end

  class InvalidActor < ServiceError
    def initialize(msg = I18n.t('services.errors.invalid_actor'))
      super
    end
  end

  class InvalidParams < ServiceError
    def initialize(msg = I18n.t('services.errors.invalid_params'))
      # :nocov:
      super
      # :nocov:
    end
  end
end
