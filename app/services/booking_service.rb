module BookingService
  class BookingError < Services::ServiceError; end

  module_function

  def create(*args)
    BookingService::Create.new(*args).call
  end
end
