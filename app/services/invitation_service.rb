module InvitationService
  class InvitationError < Services::ServiceError; end

  class EmailMissingError < InvitationError
    def initialize(msg = I18n.t('services.errors.email_missing'))
      super
    end
  end

  class InvalidToken < InvitationError
    def initialize(msg = I18n.t('services.errors.invalid_token'))
      super
    end
  end

  class AlreadyAcceptedError < InvitationError
    def initialize(msg = I18n.t('services.errors.already_accepted'))
      super
    end
  end

  class AlreadyInvitedError < InvitationError
    def initialize(msg = I18n.t('services.errors.already_invited'))
      super
    end
  end

  module_function

  def invite(*args)
    InvitationService::Invite.new(*args).call
  end

  def accept(*args)
    InvitationService::Accept.new(*args).call
  end

  def validate(*args)
    InvitationService::Validate.new(*args).call
  end
end
