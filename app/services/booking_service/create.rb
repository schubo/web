module BookingService
  class Create < BookingService::Base
    Contract Booking, Account, C::Any => C::Any
    def initialize(booking, actor)
      super(booking, actor)
    end

    def perform
      validate

      @booking.school = @booking.children.first&.school
      @booking.parent = @actor.entity
      @booking.status = :active
      @booking.save if @booking.errors.empty?
      @booking
    end

    private

    def validate
      if @booking.children.present? && @booking.children.map(&:school_id).uniq.size != 1
        @booking.errors.add(:children, 'not all of children is in the same school')
      end
    end
  end
end
