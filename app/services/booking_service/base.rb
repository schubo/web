module BookingService
  class Base < Services::Base
    Contract Booking, Account => C::Any
    def initialize(booking, actor)
      super(actor)
      @booking = booking
    end
  end
end
