module InvitationService
  class Base < Services::Base
    Contract Invitation, Account => C::Any
    def initialize(invitation, actor)
      super(actor)
      @invitation = invitation
    end
  end
end
