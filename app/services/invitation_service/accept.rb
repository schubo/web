module InvitationService
  class Accept < InvitationService::Base
    Contract Invitation, Account, C::Any => C::Any
    def initialize(invitation, actor, params)
      super(invitation, actor)
      @params = params
    end

    def perform
      validate

      if @params.dig(:entity).present?
        entity_params = @params['entity'].except(:type)
        type = @params['entity']['type']

        if type == 'school'
          entity = School.new(entity_params)
        elsif type == 'parent'
          entity = Parent.new(entity_params)
        end
      end

      ActiveRecord::Base.transaction do
        account = @invitation.account
        if account.entity.blank?
          entity.save!
          account.password = @params['password'] || ''
          account.password_confirmation = @params['password_confirmation'] || ''
          account.entity = entity
          account.save!
        end

        if @invitation.as_parent?
          @invitation.school.parents << account.entity
        end

        @invitation.accepted_at = Time.zone.now
        @invitation.save!
      end

      @invitation
    end

    private

    def validate
      InvitationService.validate(@invitation, @actor)
    end
  end
end
