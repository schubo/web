module InvitationService
  class Invite < InvitationService::Base
    Contract Invitation, Account, String => C::Any
    def initialize(invitation, actor, email)
      super(invitation, actor)
      @email = email
    end

    def perform
      raise InvitationService::EmailMissingError if @email.blank?

      raise Services::InvalidActor unless @actor.admin? || @actor.school?

      if Account.where(email: @email, admin: true).exists? ||
        Invitation.where_exists(:account, email: @email).where(invited_as: :as_school).exists? ||
        Invitation.where_exists(:account, email: @email).where(invited_as: :as_parent, school_id: @actor.entity_id).exists?
        raise InvitationService::AlreadyInvitedError
      end

      ActiveRecord::Base.transaction do
        account = Account.find_or_initialize_by(email: @email)
        account.save(validate: false) unless account.persisted?

        @invitation.account = account
        @invitation.invited_by = @actor
        raw, enc = Devise.token_generator.generate(@invitation.class, :token)
        @invitation.token = enc

        if @actor.school?
          @invitation.school_id = @actor.entity_id
          @invitation.invited_as = :as_parent
        else
          @invitation.invited_as = :as_school
        end

        @invitation.save!
      end

      InvitationMailer.invitation_instructions(@invitation).deliver_later

      @invitation
    end
  end
end
