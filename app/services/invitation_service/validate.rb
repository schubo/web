module InvitationService
  class Validate < InvitationService::Base
    def perform
      raise InvitationService::InvalidToken unless @invitation.persisted?
      raise InvitationService::AlreadyAcceptedError if @invitation.accepted_at.present?
      raise Services::InvalidActor if @invitation.invited_by == @actor

      @invitation
    end
  end
end
