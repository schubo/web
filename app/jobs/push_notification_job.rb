# :nocov:
class PushNotificationJob < ApplicationJob
  def perform(object, payload)
    return unless object.is_a?(Parent) || object.is_a?(Child)

    last_device = object.devices.last
    send_to_android(last_device, payload) if last_device.android?
  end

  private

  def send_to_android(device, payload)
    uri = URI.parse('https://fcm.googleapis.com/fcm/send')

    header = {
      'Authorization': "key=#{ENV['FCM_SERVER_KEY']}",
      'Content-Type': 'application/json'
    }
    params = {
      to: device.token,
      data: payload,
      notification: {
        title: payload[:title],
        body: payload[:body]
      }
    }

    # Create the HTTP objects
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Post.new(uri.request_uri, header)
    request.body = params.to_json

    p "sending to #{device.token} with #{payload}"

    # Send the request
    response = http.request(request)

    p "result: #{JSON.parse(response.body)}"
  end
end
# :nocov:
