class ParentSerializer < ApplicationSerializer
  attribute :id
  attribute :first_name
  attribute :last_name
  attribute :street
  attribute :street_number
  attribute :zipcode
  attribute :town
  attribute :phone_no
end
