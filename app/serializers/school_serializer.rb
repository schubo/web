class SchoolSerializer < ApplicationSerializer
  attribute :id
  attribute :name
  attribute :street
  attribute :street_number
  attribute :zipcode
  attribute :town
  attribute :phone_no
  attribute :latitude
  attribute :longitude
end
