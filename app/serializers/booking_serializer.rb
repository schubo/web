class BookingSerializer < ApplicationSerializer
  attribute :id
  attribute :date
  attribute :trip_type
  attribute :status
  belongs_to :school
  belongs_to :parent
  belongs_to :meeting_point
  has_many :children

  def date
    object.date.iso8601
  end
end
