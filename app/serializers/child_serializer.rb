class ChildSerializer < ApplicationSerializer
  attribute :id
  attribute :name
  attribute :stage
  attribute :active
  attribute :avatar_url
  belongs_to :school
  has_many :parents

  def avatar_url
    object.avatar_url
  end
end
