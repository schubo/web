class DeviceSerializer < ApplicationSerializer
  attribute :id
  attribute :token
  attribute :os
  belongs_to :owner, polymorphic: true
end
