class MeetingPointSerializer < ApplicationSerializer
  attribute :id
  attribute :name
  attribute :street
  attribute :street_number
  attribute :zipcode
  attribute :town
  attribute :latitude
  attribute :longitude
end
