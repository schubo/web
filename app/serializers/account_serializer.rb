class AccountSerializer < ApplicationSerializer
  attribute :id
  attribute :email
  belongs_to :entity, polymorphic: true
end
