class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@#{ENV.fetch('PRODUCTION_URL')}"
  layout 'mailer'
end
