class InvitationMailer < ApplicationMailer
  def invitation_instructions(invitation)
    @email = invitation.account.email
    @token = invitation.token

    mail(to: @email, subject: 'Invitation instructions')
  end
end
