class DashboardsController < ApplicationController
  def index
    authorize :Dashboard
  end
end
