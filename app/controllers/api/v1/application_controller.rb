class API::V1::ApplicationController < ActionController::API
  include Pundit

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  before_action :authenticate_account!
  before_action :set_parent

  def pundit_user
    current_account
  end

  private

  def set_parent
    @parent = current_account&.entity
  end

  def user_not_authorized
    render json: {}, status: :unauthorized
  end
end
