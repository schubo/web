class API::V1::SchoolsController < API::V1::ApplicationController
  def index
    authorize self
    render json: current_account.entity.schools
  end

  private

  def self.policy_class
    API::SchoolPolicy
  end
end
