class API::V1::BookingsController < API::V1::ApplicationController
  include ChildAuthorization

  before_action :set_booking, only: [:show, :update, :cancel, :approach, :finish]
  skip_before_action :authenticate_account!, only: [:index, :show, :approach, :finish]
  before_action :authenticate_child!, only: [:index, :show]

  def index
    authorize self
    @bookings = (@parent || @child).bookings.active
    render json: @bookings
  end

  def show
    authorize @booking, policy_class: self.class.policy_class
    render json: @booking
  end

  def create
    @booking = Booking.new(booking_params)
    authorize @booking, policy_class: self.class.policy_class
    @booking = BookingService.create(@booking, current_account)
    if @booking.errors.empty?
      render json: @booking, status: :created
    else
      render json: @booking.errors, status: :unprocessable_entity
    end
  end

  def update
    authorize @booking, policy_class: self.class.policy_class
    if @booking.update(booking_params)
      render json: @booking, status: :ok
    else
      render json: @booking.errors, status: :unprocessable_entity
    end
  end

  def cancel
    authorize @booking, policy_class: self.class.policy_class
    @booking.cancel!
    render json: {}, status: :ok
  end

  def approach
    # :nocov:
    # send notification to the parents
    PushNotificationJob.perform_later(@booking.parent, title: "Bus for #{@booking.children.map(&:name).to_sentence} is approaching!", body: 'Prepare your children')
    # :nocov:
  end

  def finish
    # :nocov:
    # send notification to the parents
    PushNotificationJob.perform_later(@booking.parent, title: "Bus for #{@booking.children.map(&:name).to_sentence} is arrived at the destination!", body: 'They arrived safely')
    @booking.finish!
    # :nocov:
  end

  private

  def set_booking
    @booking = Booking.find(params[:id] || params[:booking_id])
  end

  def booking_params
    params.require(:booking).permit(:date, :trip_type, :meeting_point_id, child_ids: [])
  end

  def self.policy_class
    API::BookingPolicy
  end
end
