class API::V1::DevicesController < API::V1::ApplicationController
  def create
    authorize self
    @device = Device.new(device_params)
    @device.owner = current_account.entity
    if @device.save
      render json: @device, status: :created
    else
      render json: @device.errors, status: :unprocessable_entity
    end
  end

  private

  def device_params
    params.require(:device).permit(:token, :os)
  end

  def self.policy_class
    API::DevicePolicy
  end
end
