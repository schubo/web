class API::V1::MeetingPointsController < API::V1::ApplicationController
  def index
    @meeting_points = MeetingPoint.all
    render json: @meeting_points
  end
end
