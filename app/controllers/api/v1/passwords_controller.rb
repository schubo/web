# :nocov:
class API::V1::PasswordsController < Devise::PasswordsController
  respond_to :json
end
# :nocov:
