class API::V1::ChildrenController < API::V1::ApplicationController
  include ChildAuthorization

  before_action :set_child, only: [:show, :update, :destroy, :pair, :board]
  skip_before_action :authenticate_account!, only: [:pair, :show, :board]
  before_action :authenticate_child!, only: [:show]

  def index
    authorize self
    @children = @parent.children
    render json: @children
  end

  def show
    authorize @child, policy_class: self.class.policy_class
    render json: @child
  end

  def create
    @child = Child.new(child_params)
    authorize @child, policy_class: self.class.policy_class
    if @child.save && (@child.parents << @parent)
      render json: @child, status: :created
    else
      render json: @child.errors, status: :unprocessable_entity
    end
  end

  def update
    authorize @child, policy_class: self.class.policy_class
    if @child.update(child_params)
      render json: @child, status: :ok
    else
      render json: @child.errors, status: :unprocessable_entity
    end
  end

  def destroy
    authorize @child, policy_class: self.class.policy_class
    @child.destroy
    render json: {}, status: :ok
  end

  def pair
    authorize @child, policy_class: self.class.policy_class
    ActiveRecord::Base.transaction do
      @child.update(active: true)
      @device = Device.new(device_params)
      @device.owner = @child
      @device.save!
    end
    token, payload = Warden::JWTAuth::TokenEncoder.new.call(sub: @child.id, scp: :child)
    response.set_header('Authorization', "Bearer #{token}")
    render json: {}, status: :ok
  rescue StandardError
    # :nocov:
    render json: @device.errors, status: :unprocessable_entity
    # :nocov:
  end

  def board
    # :nocov:
    # send notification to the parents
    @child.parents.each do |parent|
      PushNotificationJob.perform_later(parent, title: "#{@child.name} onboard!", body: 'Your children is already onboard')
    end
    # :nocov:
  end

  private

  def set_child
    @child = Child.find(params[:id] || params[:child_id])
  end

  def child_params
    params.require(:child).permit(:name, :stage, :school_id, :avatar)
  end

  def device_params
    params.require(:device).permit(:token, :os)
  end

  def self.policy_class
    API::ChildPolicy
  end
end
