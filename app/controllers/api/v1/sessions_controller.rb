class API::V1::SessionsController < Devise::SessionsController
  skip_before_action :verify_authenticity_token
  before_action :disable_session
  respond_to :json

  private

  def respond_with(resource, _opts = {})
    render json: resource, include: ['entity']
  end

  def respond_to_on_destroy
    # :nocov:
    head :no_content
    # :nocov:
  end

  def disable_session
    request.session_options[:skip] = true
  end

  def resource_name
    :account
  end
end
