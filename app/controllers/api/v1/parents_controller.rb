class API::V1::ParentsController < API::V1::ApplicationController
  def show
    authorize self
    render json: @parent
  end

  def update
    authorize self
    if @parent.update(parent_params)
      render json: @parent, status: :ok
    else
      render json: @parent.errors, status: :unprocessable_entity
    end
  end

  private

  def parent_params
    params.require(:parent).permit(:first_name, :last_name, :street, :street_number, :zipcode, :town, :phone_no)
  end

  def self.policy_class
    API::ParentPolicy
  end
end
