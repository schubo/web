class InvitationsController < ApplicationController
  def new
    authorize Invitation
  end

  def create
    authorize Invitation

    email = params[:account][:email]
    begin
      InvitationService.invite(Invitation.new, current_account, email)
      redirect_to new_invitation_path, notice: 'Invitation sent'
    rescue Services::ServiceError => e
      redirect_to new_invitation_path, alert: e.message
    end
  end

  def edit
    authorize Invitation

    @invitation = Invitation.find_or_initialize_by token: params[:token]
    begin
      InvitationService.validate(@invitation, current_account || Account.new)
    rescue Services::ServiceError => e
      flash[:alert] = e.message
      redirect_to root_path, alert: e.message
    end
  end

  def update
    authorize Invitation

    @invitation = Invitation.find_or_initialize_by token: params[:account][:token]
    begin
      InvitationService.accept(@invitation, current_account || Account.new, account_params)
      redirect_to new_account_session_path, notice: 'Registered Successfully'
    rescue InvitationService::InvalidToken, InvitationService::AlreadyAcceptedError, Services::InvalidActor => e
      redirect_to root_path, alert: e.message
    rescue Services::ServiceError => e
      flash[:alert] = e.message
      render action: 'edit'
    end
  end

  protected

  def account_params
    entity_params = { entity: [:type, :name, :first_name, :last_name, :street, :street_number, :zipcode, :town, :phone_no] }
    params.require(:account).permit(:password, :password_confirmation, entity_params)
  end
end
