module ChildAuthorization
  extend ActiveSupport::Concern

  def authenticate_child!
    token = request.headers['Authorization'].split('Bearer').second
    payload = Warden::JWTAuth::TokenDecoder.new.call(token.strip)
    raise 'not a child token' unless payload['scp'] == 'child'

    @child = Child.find payload['sub']
  rescue StandardError
    authenticate_account!
  end
end
