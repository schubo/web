class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def pundit_user
    current_account
  end

  protected

  def after_sign_in_path_for(resource)
    dashboards_path
  end

  def after_sign_out_path_for(resource_or_scope)
    # :nocov:
    new_account_session_path
    # :nocov:
  end

  private

  def user_not_authorized
    flash[:alert] = 'You are not authorized to perform this action.'
    redirect_to root_path
  end
end
